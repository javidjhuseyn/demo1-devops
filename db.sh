#!/usr/bin/env bash

# mysql-server installation
sudo apt-get update -y && sudo apt-get install -qq mysql-server -y


success() {
  echo -e '\e[32m'$1'\e[m';
}

DB_NAME=petclinic
DB_USER=petclinic
DB_PASS=petclinic
ROOT_PASS=root

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $ROOT_PASS"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $ROOT_PASS"

MYSQL=`which mysql`

# user and database set up
Q1="CREATE DATABASE IF NOT EXISTS $DB_NAME;"
Q2="CREATE USER IF NOT EXISTS '$DB_USER'@'192.168.33.%' IDENTIFIED BY '$DB_PASS';"
Q3="GRANT ALL ON $DB_NAME.* TO '$DB_USER'@'192.168.33.%';"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

# executing sql commands
$MYSQL -uroot -p$ROOT_PASS -e "$SQL"

# printing success message
success "Database and user created"

# replacing bind address to be able to connect to database
sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

# here - restarting database to persist changes 
sudo service mysql restart
