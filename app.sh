#!/bin/bash

# jdk 14 installation
sudo apt-get update && sudo apt-get install openjdk-14-jdk -y

# cloning repository 

repo="https://oauth2:iBYS_mnuoJU8dsESmTCB@gitlab.com/javidjhuseyn/az-devops1.git"
dir_name=$(basename "https://oauth2:iBYS_mnuoJU8dsESmTCB@gitlab.com/javidjhuseyn/az-devops1.git" .git)

if [[ -d "$dir_name" ]]; then
    cd $dir_name
    git pull
else
    git clone "$repo" && cd $dir_name
fi

# making mvnw executable
sudo chmod +x mvnw


# jar file generation for systemd service
sudo ./mvnw clean package

